package com.paysafe.simplelogin

import com.paysafe.simplelogin.di.DaggerPaysafeAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * Created by Lyubomir Dokuzov
 */
class PaysafeApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerPaysafeAppComponent.builder().application(this).build()
        component.inject(this)
        return component
    }
}
