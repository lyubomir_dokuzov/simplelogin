package com.paysafe.simplelogin.home

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.paysafe.simplelogin.R
import com.paysafe.simplelogin.base.Constant.KEY_USER
import com.paysafe.simplelogin.base.PaysafeBaseActivity
import com.paysafe.simplelogin.databinding.ActivityHomePageBinding
import com.paysafe.simplelogin.model.UserModelImpl
import com.paysafe.simplelogin.provided.UserInfoProvider

class HomeActivity : PaysafeBaseActivity() {

    private var homeViewModel: HomeViewModel? = null
    private var binding: ActivityHomePageBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProviders.of(this@HomeActivity, viewModelFactory).get(HomeViewModel::class.java) // do whatever ..
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home_page)

        val user = intent?.extras?.getParcelable<UserModelImpl>(KEY_USER)
        user?.let {
            val avatar = UserInfoProvider.getUserAvatar(this, it)
            binding?.ivAvatar?.setImageBitmap(avatar)
        }

    }
}
