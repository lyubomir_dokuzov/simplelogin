package com.paysafe.simplelogin.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.paysafe.simplelogin.provided.UserModel;

/**
 * Created by Lyubomir Dokuzov
 */
public class UserModelImpl implements UserModel, Parcelable {

    private String username;
    private String password;

    public UserModelImpl() {

    }

    public UserModelImpl(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    protected UserModelImpl(Parcel in) {
        username = in.readString();
        password = in.readString();
    }


    public void setUsername(final String username) {
        this.username = username;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public static final Creator<UserModelImpl> CREATOR = new Creator<UserModelImpl>() {
        @Override
        public UserModelImpl createFromParcel(Parcel in) {
            return new UserModelImpl(in);
        }

        @Override
        public UserModelImpl[] newArray(int size) {
            return new UserModelImpl[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(username);
        dest.writeString(password);
    }
}
