package com.paysafe.simplelogin.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.paysafe.simplelogin.R
import com.paysafe.simplelogin.base.Constant.KEY_USER
import com.paysafe.simplelogin.base.PaysafeBaseActivity
import com.paysafe.simplelogin.databinding.ActivityLoginBinding
import com.paysafe.simplelogin.home.HomeActivity

class LoginActivity : PaysafeBaseActivity() {

    private var binding: ActivityLoginBinding? = null
    private var loginViewModel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        loginViewModel = ViewModelProviders.of(this@LoginActivity, viewModelFactory).get(LoginViewModel::class.java)

        loginViewModel?.let {
            it.errorMessageEvent.observe(this@LoginActivity, Observer { message ->
                Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
            })

            it.userEvent.observe(this@LoginActivity, Observer { user ->
                val home = Intent(this, HomeActivity::class.java).apply { putExtra(KEY_USER, user) }
                startActivity(home)
            })

        }

        binding?.let { it.viewModel = loginViewModel }
    }

}

