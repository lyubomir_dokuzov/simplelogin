package com.paysafe.simplelogin.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paysafe.simplelogin.model.UserModelImpl
import com.paysafe.simplelogin.provided.AuthenticatorCallback
import com.paysafe.simplelogin.provided.UserAuthenticationProvider
import javax.inject.Inject

/**
 * Created by Lyubomir Dokuzov
 *
 */
class LoginViewModel @Inject constructor(private val userAuthenticationProvider: UserAuthenticationProvider) : ViewModel(), AuthenticatorCallback {

    val errorMessageEvent: MutableLiveData<String> = MutableLiveData()
    val userEvent: MutableLiveData<UserModelImpl> = MutableLiveData()
    var userModel: UserModelImpl = UserModelImpl()
    var username: String = ""
    var pass: String = ""

    fun login() {
        userModel.username = username
        userModel.password = pass
        userAuthenticationProvider.authenticateUser(userModel, this)
    }

    override fun onError(error: Exception?) {
        error?.let {
            errorMessageEvent.postValue(it.message)
        } ?: errorMessageEvent.postValue("Something went wrong")
    }

    override fun onSuccess(success: Boolean) {
        userEvent.postValue(userModel)
    }

}