package com.paysafe.simplelogin.provided;

/*
 * User model contract
 * */
public interface UserModel {

    String getUsername();

    String getPassword();
}
