package com.paysafe.simplelogin.provided;

/*
 * Authentication API contract
 * */
public interface AuthenticatorCallback {
    void onSuccess(Boolean success);

    void onError(Exception error);
}
