package com.paysafe.simplelogin.provided;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.paysafe.simplelogin.R;

public class UserInfoProvider {

    /*
     * getUserAvatar: Simulates a request for getting the user avatar, returns a ready to use Bitmap
     * */
    public static Bitmap getUserAvatar(Context context, UserModel userModel) throws IllegalArgumentException {
        if (TextUtils.isEmpty(userModel.getUsername()) || TextUtils.isEmpty(userModel.getPassword())) {
            throw new IllegalArgumentException("User model is not valid");
        }
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.avatar);
    }

}
