package com.paysafe.simplelogin.provided

import android.text.TextUtils
import java.util.*

open class UserAuthenticationProvider {

    /*
     * authenticateUser: Simulates a network request for user authentication.
     * */
    open fun authenticateUser(userModel: UserModel?, authenticatorCallback: AuthenticatorCallback?) {
        Timer().schedule(object : TimerTask() {
            override fun run() {

                userModel?.let {
                    if (TextUtils.isEmpty(userModel.username) || TextUtils.isEmpty(userModel.password)) {
                        authenticatorCallback?.onError(IllegalArgumentException("Username or password cannot be null!"))
                        return
                    }

                    authenticatorCallback?.onSuccess(true)

                } ?: authenticatorCallback?.onError(IllegalArgumentException("User cannot be null!"))


            }
        }, 300)
    }
}
