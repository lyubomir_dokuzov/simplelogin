package com.paysafe.simplelogin.di

import com.paysafe.simplelogin.PaysafeApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Lyubomir Dokuzov
 */
@Singleton
@Component(modules = [
    PaysafeAppModule::class,
    AndroidSupportInjectionModule::class,
    ContributesActivityModule::class
])
interface PaysafeAppComponent : AndroidInjector<PaysafeApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: PaysafeApplication): Builder

        fun build(): PaysafeAppComponent
    }
}