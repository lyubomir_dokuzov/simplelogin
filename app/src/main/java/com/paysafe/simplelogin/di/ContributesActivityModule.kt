package com.paysafe.simplelogin.di

import android.app.Activity
import com.paysafe.simplelogin.home.HomeActivity
import com.paysafe.simplelogin.login.LoginActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Lyubomir Dokuzov
 *
 */
@Module
abstract class ContributesActivityModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [ContributesLoginActivityModule::class])
    abstract fun contributeLoginActivity(): LoginActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ContributesHomeActivityModule::class])
    abstract fun contributeHomeActivity(): HomeActivity

}

@Module(includes = [ViewModelFactoryModule::class])
abstract class ContributesLoginActivityModule {
    @Binds
    @PerActivity
    abstract fun contributeLoginActivity(activity: LoginActivity): Activity
}

@Module(includes = [ViewModelFactoryModule::class])
abstract class ContributesHomeActivityModule {
    @Binds
    @PerActivity
    abstract fun contributeHomeActivity(activity: HomeActivity): Activity
}


