package com.paysafe.simplelogin.di;

import com.paysafe.simplelogin.home.HomeViewModel;
import com.paysafe.simplelogin.login.LoginViewModel;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Lyubomir Dokuzov
 */
@Module
public abstract class ViewModelKeysModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel bindsHomeViewModel(HomeViewModel homeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindsLoginViewModel(LoginViewModel viewModel);
}
