package com.paysafe.simplelogin.di;

import com.paysafe.simplelogin.provided.UserAuthenticationProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lyubomir Dokuzov
 */
@Module
public class PaysafeAppModule {

    @Singleton
    @Provides
    public static UserAuthenticationProvider providesUserAuthenticationProvider() {
        return new UserAuthenticationProvider();
    }
}
