package com.paysafe.simplelogin.di;

import java.util.Map;

import javax.inject.Provider;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Lyubomir Dokuzov
 */
@Module(includes = ViewModelKeysModule.class)
public class ViewModelFactoryModule {
    @Provides
    @PerActivity
    public static ViewModelProvider.Factory provideViewModelProviderFactory(final Map<Class<? extends ViewModel>, Provider<ViewModel>> creators) {
        return new ViewModelProviderFactory(creators);
    }
}
