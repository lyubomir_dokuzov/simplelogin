package com.paysafe.simplelogin.di

import androidx.lifecycle.ViewModel
import dagger.MapKey
import javax.inject.Scope
import kotlin.reflect.KClass

/**
 * Created by Lyubomir Dokuzov
 *
 */

@Scope
annotation class PerActivity