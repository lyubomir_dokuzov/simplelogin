package com.paysafe.simplelogin.login

import androidx.lifecycle.Observer
import com.paysafe.simplelogin.BaseUnitTest
import com.paysafe.simplelogin.model.UserModelImpl
import com.paysafe.simplelogin.provided.AuthenticatorCallback
import com.paysafe.simplelogin.provided.UserAuthenticationProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Lyubomir Dokuzov
 */
@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest : BaseUnitTest() {

    @Mock
    lateinit var authenticatorProvider: UserAuthenticationProvider
    @Mock
    private lateinit var errorObserver: Observer<String>
    @Mock
    private lateinit var userObserver: Observer<UserModelImpl>

    lateinit var loginViewModel: LoginViewModel


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        loginViewModel = LoginViewModel(authenticatorProvider).also {
            it.errorMessageEvent.observeForever(errorObserver)
            it.userEvent.observeForever(userObserver)
        }

        Mockito.doAnswer { invocation -> // Repeating the logic ... but the implementation of the #provided file isn't flexible and isn't to be modified
            val userModelImpl = invocation.getArgument(0) as UserModelImpl
            val authenticatorCallback = invocation.getArgument(1) as AuthenticatorCallback

            if (userModelImpl.username == null || userModelImpl.username.isEmpty()
                    || userModelImpl.password == null || userModelImpl.password.isEmpty()) {
                authenticatorCallback.onError(IllegalArgumentException("Username or password cannot be null!"))
            } else {
                authenticatorCallback.onSuccess(true)
            }

        }.`when`(authenticatorProvider).authenticateUser(any(), any())

    }

    @Test
    fun test_login_with_valid_credentials() {
        loginViewModel.username = "user_name"
        loginViewModel.pass = "password"

        loginViewModel.login()
        Mockito.verify(userObserver).onChanged(any())
    }

    @Test
    fun test_login_with_empty_pass() {
        loginViewModel.username = "user_name"
        loginViewModel.pass = ""

        loginViewModel.login()
        Mockito.verify(errorObserver).onChanged("Username or password cannot be null!")
        Mockito.verify(userObserver, Mockito.never()).onChanged(any())
    }

    @Test
    fun test_login_with_empty_user_name() {
        loginViewModel.username = ""
        loginViewModel.pass = "password"

        loginViewModel.login()
        Mockito.verify(errorObserver).onChanged("Username or password cannot be null!")
        Mockito.verify(userObserver, Mockito.never()).onChanged(any())
    }


}