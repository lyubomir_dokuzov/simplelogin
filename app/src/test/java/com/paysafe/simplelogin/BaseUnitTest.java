package com.paysafe.simplelogin;

import org.junit.Rule;
import org.junit.rules.TestRule;

/**
 * Created by Lyubomir Dokuzov
 */
public class BaseUnitTest {
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

}
